# README

#### TITLE OF MANUCRIPT

AUTHORS

STATUS

---

This directory contains instructions for building this article from source code using R bookdown, and Pandoc. For easy reproduction a Dockerfile is provided which enables the build to be made in of a container with all dependencies.

---

## File and directory structure

#### `README.md`
This README is written in markdown. Edit this with the article details, instrucions for building, and a description of the data. 

#### `bibliography.bib`
The article bibliography written in BibTex. Most reference managers allow export in BiBTex format, use text writer to copy these to this file. inserting citations in the text is done as `@my-bibtex-code` or [`@my-bibtex-code`, `@my-other-bibtex-code`], where `my-bibtex-code` is usually the first entry in each bibtex item.  

#### `index.Rmd`
The main article text written in Rmarkdown (see this [guide](https://bookdown.org/yihui/bookdown/markdown-syntax.html) to syntax). Metadata (written in yaml) should be edited in the front matter of this document (this information will be used to create the title page). Sub-headings are sections into which text, figures and tables should be added. Author contributions and acknowledgements are inserted at the end of the document automatically from the metadata. The bibliography is automatically added in section 'References' (note this MUST be at end of document), using the tags (`@my-bibtex-code`) within the text and `bibliography.bib`. If the corresponding BibTex item is not found then a '??' is inserted. 

Figure and table numbering is automatically done using tags; `Figure \@ref(fig:fig-study-site)`. For figures the procedure is create an image (png or eps) with an informative name e.g., `fig-study-site.png`, and store in `/figures`. If possible also use the same for the code file e.g., `fig-study-site.R` stored in `/code`. Insert a 'code chunk' using the figure basename as the tag for the chunk, fill in the file name, and the caption text tag, and below the code chunk write the caption for the figure, preceded by `(ref:cap-study-site)`, like;

```
{r, study-site, fig.cap='(ref:cap-study site)'}

knitr::include_graphics("figures/study-site.png")

(ref:cap-study-site) Map showing study area.

```
Inserting tables uses a similar syntax `Table \@ref(tab:tab-study-site)`. Create code to make the table using data stored in `/data`, again using the same tag-name e.g., `tab-study-site.R`, export table in markdown to `/tables`, e.g., `tab-study-site.md`, and add as;

```
{r tab-study-site, child = 'tables/tab-study-site.md'}
```
Numbered math expressions (equations= are also possible using LaTeX syntax, see [guide](https://bookdown.org/yihui/bookdown/markdown-syntax.html#math-expressions) for details.

#### `_output.yml`
This is a configuration file that sets the bookdown output options, e.g., html, pdf and word.

#### `_bookdown.yml`
This is a configuration file with bookdown style options.

#### `/config`
This directory contains a css style sheet (`custom.css`), citation styles (`*.csl`), word document style template (`word-template.docx`), and TeX configuartion (`preamble.tex`).

#### `/code`            
This directory contains the code to transform data, perform analysis, and make figures and tables. Organisation is flexible, but for example different programming languages may be placed in different folders, e.g., `/R`, `/Python`, `Shell`. In general aim for relatively self-contained code files with human-readable names, e.g., `parse-prepare-data.R`, `tab-study-site.R`, and `fig-study-site.R`.

Excecuting the code when building the article can be done by inserting a code chunk in the text, this can be all at the start of the text or through the text e.g., at the start of a section. Chunks may be a different language, set by the first entry in a chunk header e.g., `python`. All code within a chunk will be executed, and any objects create will be available in the environment i.e., when building the article. Object values can be inseted in the text as `'r n'` and appear as 4.

```
## Methods

{r parse-prepare-data, include=FALSE}
# Parse data files 
source('code/R/parse-prepare-data.R')

# Calculate something
n <- 2 + 2

```

#### `/data`
This includes all data, again organisation is flexible, and sub-directories can be used as needed, just remember any changes to directories means updating code in document. Again try to use human-readable names (e.g., `position-study-sites`) for data files, and transportable formats, such as `.csv`.

#### `/figures`
This directory stores figures for the article, preferably as `.png`, and/or `.eps`. Files from graphics and GIS software used for tweaking figures and maps can also go here. 

#### `/tables`
Here goes tables, preferably in markdown (`.md`) syntax.

#### `/_article-output`
The compiled article output will be made here. This will not appear until article is built. 


---

This article was produced using [R](www.cran.org), [Rstudio](https://www.rstudio.com/), [rmarkdown](https://github.com/rstudio/rmarkdown), and the [bookdown](https://github.com/rstudio/bookdown).